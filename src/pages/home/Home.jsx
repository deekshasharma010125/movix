import React from 'react';
import './style.scss';
import HeroBanner from './heroBanner/HeroBanner';
import Trending from './trending/Trending';

const Home = () => {
	return (
		<>
			<div>
				<HeroBanner />
			</div>
			<Trending />
		</>
	);
};

export default Home;
